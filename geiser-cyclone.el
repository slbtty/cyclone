;;; geiser-cyclone.el --- Cyclone Scheme's implementation of the geiser protocols  -*- lexical-binding: t; -*-

;; Author: shenlebantongying <shenlebantongying@gmail.com>
;; Maintainer: Jose A Ortega Ruiz <jao@gnu.org>
;; Keywords: languages, cyclone, scheme, geiser
;; Homepage: https://gitlab.com/emacs-geiser/cyclone
;; Package-Requires: ((emacs "26.1") (geiser "0.19"))
;; SPDX-License-Identifier: BSD-3-Clause
;; Version: 0.1

;;; Commentary:

;; This package provides support for Cyclone scheme in geiser.

;;; Code:

(require 'geiser)

(require 'geiser-connection)
(require 'geiser-syntax)
(require 'geiser-custom)
(require 'geiser-base)
(require 'geiser-eval)
(require 'geiser-edit)
(require 'geiser-log)
(require 'geiser-impl)
(require 'geiser-repl)

(require 'compile)
(require 'info-look)

(eval-when-compile (require 'cl-lib))

(defgroup geiser-cyclone nil
  "Customization for Geiser's Cyclone Scheme flavour."
  :group 'geiser)

(geiser-custom--defcustom geiser-cyclone-binary
  "icyc"
  "Name to use to call the Cyclone Scheme executable when starting a REPL."
  :type '(choice string (repeat string)))

;;; REPL support:

(defun geiser-cyclone--binary ()
  "Return path to Chez scheme binary."
  (if (listp geiser-cyclone-binary)
      (car geiser-cyclone-binary)
    geiser-cyclone-binary))

;; ;;; REPL startup

(defun geiser-cyclone--version (binary)
  "Use BINARY to find Cyclone scheme version."
  (process-lines binary "-vn"))

(defun geiser-cyclone--startup (_remote)
  "Initialize a Cyclone REPL."
  (let ((geiser-log-verbose-p t))
    (compilation-setup t)
    (geiser-eval--send/wait "(newline)")))

(defvar geiser-cyclone-scheme-dir
  (expand-file-name "src" (file-name-directory load-file-name))
  "Directory where the Cyclone scheme geiser modules are installed.")

(defun geiser-cyclone--parameters ()
  "Return a list with all parameters needed to start Cyclone Scheme."
 `("-s" ,(expand-file-name "geiser/cyclone.scm" geiser-cyclone-scheme-dir)))

(defconst geiser-cyclone--prompt-regexp "cyclone> ")


(defun geiser-cyclone--geiser-procedure (proc &rest args)
  (let ((form (mapconcat 'identity args " ")))
       (format "(geiser:eval %s %s)" proc form)))

;;; Implementation definition:

(define-geiser-implementation cyclone
  (binary geiser-cyclone--binary)
  (arglist geiser-cyclone--parameters)
  (repl-startup geiser-cyclone--startup)
  (prompt-regexp geiser-cyclone--prompt-regexp)
  (version-command geiser-cyclone--version)
  (marshall-procedure geiser-cyclone--geiser-procedure)
)

;;;###autoload
(geiser-implementation-extension 'cyclone "scm")

;;;###autoload
(geiser-activate-implementation 'cyclone)

;;;###autoload
(autoload 'run-cyclone "geiser-cyclone" "Start a Geiser Cyclone REPL." t)

;;;###autoload
(autoload 'switch-to-cyclone "geiser-cyclone"
  "Start a Geiser Chez REPL, or switch to a running one." t)


(provide 'geiser-cyclone)
;;; geiser-cyclone.el ends here
