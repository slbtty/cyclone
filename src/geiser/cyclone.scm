;; -*- geiser-scheme-implementation: cyclone -*-

(define (write-to-string form)
  (let ((out (open-output-string)))
    (write form out)
    (get-output-string out)))

(define (geiser:eval proc form . rest)
  (write ; to standard output (to comint)
    `((result ,(write-to-string rest)))))
